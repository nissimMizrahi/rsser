var parseString = require('xml2js').parseString

const fs = require("fs")

//`${__dirname}\\Feeder-2020-03-30.opml`

function extractArticles(groups)
{
    //console.log("feeds", groups)
    return groups.reduce((acc, group) => {
        //console.log(group["$"].title || "")
        if(group.outline) acc.push(...extractArticles(group.outline))
        else acc.push({
            title: group["$"].title,
            url: group["$"].xmlUrl
        })
        return acc
    }, [])
}

exports.path2file = (path) => {
    return path.replace(/^.*[\\\/]/, '').split(".")[0]
}

exports.getInterestingWords = function getInterestingWords(path)
{
    return JSON.parse(fs.readFileSync(path || `${__dirname}/interesting_topics.json`).toString())
}

exports.getFeeds = function getFeeds(filename)
{
    return new Promise((resolve, reject) => {
        parseString(fs.readFileSync(filename), (err, items) => {

            if (err) {
              return reject(err)
            }
          
            var groups = items.opml.body[0].outline
            resolve(extractArticles(groups))
        });
    })
}

exports.getFileLinesWithNoComments = function(path)
{
    let text = fs.readFileSync(path).toString()
    return text
    .split("\n")
    .filter(line => line.length > 0 && line[0] != "#")
    .map(line => line.toLowerCase())
}

exports.cleanHtml = function cleanHtml(html)
{
    return html
        .replace(/<head [\S\s]*?>([\S\s]*?)<\/head>/g, "")
        .replace(/<script[\S\s]*?>[\S\s]*?<\/script>/g, "")
        .replace(/<style[\S\s]*?>[\S\s]*?<\/style>/g, "")
        .replace(/<meta[\S\s]*?>[\S\s]*?>/g, "")
        .replace(/<link[\S\s]*?>[\S\s]*?>/g, "")
        .replace(/<noscript[\S\s]*?>[\S\s]*?<\/noscript>/g, "")
        .replace(/<iframe[\S\s]*?>[\S\s]*?<\/iframe>/g, "")
        .replace(/<head[\S\s]*?>[\S\s]+<\/head>/g, "")
        .replace(/<[\S\s]*?>/g, "")
        .replace(/\s+/g, " ")
        .replace(/\s+/g, " ")
}