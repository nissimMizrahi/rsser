const { Rsser, fromOML } = require("./Rsser")
const { getFeeds, cleanHtml, getInterestingWords } = require("./Utlis")

const { Articler } = require("./Articler")

const { ArticleParser } = require("./articleParser")

function getNewArticles(articler)
{
    articler.getNewInterestingArticles().then(articles => {
        process.send({articles: articles})

        setTimeout(() => getNewArticles(articler), 1000 * 5)
    }).catch(() => {
        setTimeout(() => getNewArticles(articler), 1000 * 5)
    })
}

var articler = null

process.on("message", (msg) => {

    if(msg.type == "start")
    {
        const {path} = msg

        new Articler(path, `${__dirname}/interesting_topics.json`).then(p_articler => {
            articler = p_articler
            getNewArticles(articler)
        })
    }
    else if(msg.type == "pause"){
        console.log("stoooooppppp")
        articler.pause()
    }
    else if(msg.type == "resume"){
        articler.resume()
    }
    
})






