const { getFeeds, cleanHtml, getFileLinesWithNoComments } = require("./Utlis")

const axios = require("axios")
const natural = require("natural")

const fs = require("fs")

exports.ArticleParser = class Parser {
    constructor() {

        this.client = axios.create({timeout: 5000})

        this.blackList = {}

        let tokenizer = new natural.WordTokenizer()

        getFileLinesWithNoComments(`${__dirname}/minimal-stop.txt`).map(line => {
            tokenizer.tokenize(line).map(word => {
                this.blackList[word] = true
            })
        })
        getFileLinesWithNoComments(`${__dirname}/terrier-stop.txt`).map(line => {
            tokenizer.tokenize(line).map(word => {
                this.blackList[word] = true
            })
        })
    }

    getWord(tfidf, word)
    {
        return new Promise((resolve, reject) => {
            tfidf.tfidfs(word, (i, measure) => {
                resolve({word: word, grade: measure});
            });
        })
    }

    getTfidf(link)
    {
        return new Promise((resolve, reject) => {
            this.client.get(link)
            .then(res => {
                let text = cleanHtml(res.data).toLowerCase()
                
                let tfidf = new natural.TfIdf();
                let tokenizer = new natural.WordTokenizer()


                let tokens = tokenizer.tokenize(text)
                .filter(word => !this.blackList[word])

                tfidf.addDocument(tokens);
                resolve(tfidf)
              //resolve(tfidf.listTerms(0))
            }).catch(reject)
        })
    }

    parseLinkWithWords(link, words)
    {
        return new Promise((resolve, reject) => {
            
            this.getTfidf(link)
            .then(tfidf => {
                Promise.all(words.map(word => this.getWord(tfidf, word)))
                .then(resolve)
            }).catch((err) => {
                resolve([])
            })
        }) 
    }

    parseLink(link)
    {
        return new Promise((resolve, reject) => {
            
            this.getTfidf(link).catch(reject)
            .then(tfidf => {
                resolve(tfidf.listTerms(0))
            }).catch(() => resolve([]))
        })
    }
}