let Parser = require('rss-parser');
let { getFeeds } = require("./Utlis")

class Rsser{

    constructor(feeds){
        this.feeds = feeds
        this.visitedArticles = {}    
        this.parser = new Parser();
    }

    addFeed(url){
        return new Promise((feedResolve, feedReject) => {

            let exists = this.feeds.find(feed => feed.url == url)
            if(exists) return feedReject("feed already parsed")

            this.parser.parseURL(url).then((singleFeed => {
                
                this.feeds.push({
                    title: singleFeed.title,
                    url: url
                })
                feedResolve()

            })).catch(feedResolve)
        })
    }
    setFeeds(feeds){
        this.feeds = feeds
    }

    clearFeeds(){
        this.feeds = []
    }

    parseFeed(feed){

        return new Promise((feedResolve, feedReject) => {
            this.parser.parseURL(feed.url).then((singleFeed => {
                feedResolve(singleFeed.items.filter(item => !this.visitedArticles[item.link]).map(item => {
                    this.visitedArticles[item.link] = true
                    return item
                }))
            })).catch((err) => feedResolve(null))
        })
    }

    getNewArticles(){
        return new Promise((resolve, reject) => {
            Promise.all(
                this.feeds.map(this.parseFeed.bind(this))
            ).then((arr) => {
                resolve(
                    arr
                    .filter(obj => Boolean(obj))
                    .reduce((acc, arr2) => {
                        acc.push(...arr2)
                        return acc
                    }, 
                []))
            }).catch(reject)
        })
    }
}

exports.Rsser = Rsser

exports.fromOML = function(file)
{
    return new Promise((resolve, reject) => {
        getFeeds(file).then(feeds => {
            resolve(new Rsser(feeds))
        }).catch(reject)
    })
}