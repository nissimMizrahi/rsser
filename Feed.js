const { Feed } = require("feed")

exports.Feed = class feed {
    constructor()
    {
        this.feed = new Feed({
            title: "Feed Title",
            description: "This is my personal feed!",
            id: "http://example.com/",
            link: "http://example.com/",
            generator: "awesome", // optional, default = 'Feed for Node.js'
            author: {
              name: "John Doe",
              email: "johndoe@example.com",
              link: "https://example.com/johndoe"
            }
        });
    }

    addArticle(article)
    {
        this.feed.addItem(article)
    }

    renderRss()
    {
        return this.feed.rss2()
    }
}