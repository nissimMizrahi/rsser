const { Rsser, fromOML } = require("./Rsser")
const { getFeeds, getInterestingWords, path2file } = require("./Utlis")

const { ArticleParser } = require("./articleParser")

exports.Articler = class Articler{
    constructor(feeds_path, interesting_words_path)
    {
        return new Promise((resolve, reject) => {
            this.parser = new ArticleParser()
            this.rsser = null

            this.feeds_path = feeds_path
            this.interesting_words_path = interesting_words_path

            this.higest_score = 0
            this.avg_score = 0
            this.avg_size = 0

            this.working = true
    
            fromOML(feeds_path).then(rsser => {
                this.rsser = rsser
                resolve(this)
            }).catch(reject)
        })
    }

    threshold(){
        return this.avg_score + (this.higest_score - this.avg_score) / (Math.max(this.higest_score, 4))
    }

    pause(){
        this.working = false
    }
    resume(){
        this.working = true
    }

    getNewInterestingArticles(){

        return new Promise((resolveMain, rejectMain) => {

            if(!this.working) return rejectMain("paused")

            getFeeds(this.feeds_path)
            .then(feeds => {
                
                this.rsser.setFeeds(feeds)
                this.interesting_words = getInterestingWords(this.interesting_words_path)
        
                this.rsser.getNewArticles().then(articles => {
                
                    if(articles.length == 0) return resolveMain([])

                    let table = {
                        group: path2file(this.feeds_path),
                        articles: articles.length,
                        avg_score: this.avg_score,
                        avg_size: this.avg_size,
                        higest_score: this.higest_score,
                        threshold: this.threshold()
                    }

                    console.table([table])
                    
                    let promises = articles/*.slice(Math.max(articles.length - 10, 0))*/.map((article) => {
                        return new Promise((resolve, reject) => {
                            this.parser.parseLinkWithWords(article.link, this.interesting_words)
                            .then((res) => {

                                let grade = res.reduce((acc, grade) => {
                                    return acc + grade.grade
                                }, 0)
            
                                resolve({article: article, grade: grade})
                            }).catch(() => resolve({}))
                        })
                    })
            
                    Promise.all(promises).then((res) => {
                        
                        if(res.length == 0) return resolveMain([])

                        let avg_grade = res.reduce((acc, {grade}) => {
                            return acc + grade
                        }, 0) / res.length

                        this.higest_score = res.reduce((acc, {grade}) => {
                            return acc > grade ? acc : grade
                        }, this.higest_score)
            
                        this.avg_score = (this.avg_score * this.avg_size + avg_grade * res.length) / (this.avg_size + res.length)
                        this.avg_size += res.length
                        
                        resolveMain(res.filter(({grade}) => {
                            //console.log(grade, this.avg_score, this.avg_size, this.higest_score, this.avg_score + (this.higest_score - this.avg_score) / 2)
                            return grade > this.threshold()
                        }).map(art => art.article))
                    }).catch(rejectMain)
                })
            }).catch(rejectMain)
        })
    }
}