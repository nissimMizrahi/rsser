var cp = require("child_process")
var fs = require("fs")

var natural = require("natural")
var { Feed } = require("./Feed")

var { path2file } = require("./Utlis")

let feeds_dir = `${__dirname}/feeds`
let TOTAL_ARTICLES = 100
let g_articles = []

let tokenizer = new natural.WordTokenizer()

let handleArticleForDad = () => null


function handleArticles(articles, group)
{

    let to_push = []

    articles.forEach((article, index) => {
        let tokens = tokenizer.tokenize(article.title)


        let unpushable = ([...articles.slice(index + 1), ...g_articles]).find(article2 => {
            let tokens2 = tokenizer.tokenize(article2.title)

            let score = natural.JaroWinklerDistance(tokens.join(" "), tokens2.join(" "))
            return (score > 0.9) 
        })
        
        if(!unpushable) to_push.push(article)
    })

    if(to_push.length > 0)
    {
        g_articles.push(...to_push)
        g_articles = g_articles.slice(Math.max(0, g_articles.length - TOTAL_ARTICLES))
    
        console.table(to_push.map(article => {

            handleArticleForDad(article)

            return {
                group: group,
                article: article.title
            }
        }))
    }
}
function initNodes(){

    return new Promise((resolve, reject) => {
        fs.readdir(feeds_dir, function (err, files) {
            if (err) {
              console.error("Could not list the directory.", err);
              process.exit(1);
            }
          
            resolve(files.map((file, index) => {
                console.log(`${feeds_dir}/${file}`)
                let node = cp.fork(`${__dirname}/ArticlerNode`)
                
                node.on("message", (msg) => {
                    const {
                        articles
                    } = msg
        
                    if(articles.length > 0)
                        handleArticles(articles, path2file(file))
                })
        
                node.send({
                    type: "start",
                    path: `${feeds_dir}/${file}`
                })
                

                return node
            }))
        })
    })
}


var express = require('express');
var app = express();

initNodes().then(nodes => {

    console.log("nodes")

    app.get('/feed', function (req, res) {

        //console.log(feed.renderRss())
    
        let feed = new Feed()
    
        g_articles.forEach(article => {
            feed.addArticle(article)
        })
    
        res.status(200).send(feed.renderRss());
    })
    
    app.get('/feed/json', function (req, res) {
        res.status(200).json(g_articles);
    })
    
    app.listen(8080)
    
    process.on("message", msg => {
        const {
            command
        } = msg
    
        if(command == "start-auto")
        {
            handleArticleForDad = (article) => {
                process.send({article: article})
            }
        }
        else if(command == "stop-auto")
        {
            handleArticleForDad = () => null
        }
    
        else if(command == "pause")
        {
            nodes.forEach(node => {
                node.send({type: "pause"})
            })
        }
        else if(command == "resume")
        {
            nodes.forEach(node => {
                node.send({type: "resume"})
            })
        }
    })
})

